SUDO=''

if [ $(id -u) != 0 ]; then
  SUDO='sudo'
fi

if ! hash docker 2>/dev/null; then
  echo "\"docker\" executable not found. Please install docker."
  exit 1
fi

if ! hash docker-compose 2>/dev/null; then
  echo "\"docker-compose\" executable not found. Please install docker-compose."
  exit 1
fi

# check kernel parameter
# check if vm.max_map_count is at least 262144 on host
# this is needed for elasticsearch >5.x
ACTUAL_MAX_MAP_COUNT=$(sysctl -n vm.max_map_count)
MINIMAL_MAX_MAP_COUNT=262144

if [ "$ACTUAL_MAX_MAP_COUNT" -lt "$MINIMAL_MAX_MAP_COUNT" ]; then
  echo "vm.max_map_count is too low. This is a kernel parameter used by java which is used by elasticsearch. Elasticsearch won't run if vm.max_map_count is too low."
  while true; do
    read -p "Do you wish to increase vm.max_map_count to 262144? " yn
    case $yn in
        [Yy]* ) $SUDO sysctl -w "vm.max_map_count=262144"; echo "To make this change permanent after restart: Add \"vm.max_map_count=262144\" to \"/etc/sysctl.conf\""; break;;
        [Nn]* ) exit 1;;
        * ) echo "Please answer yes or no.";;
    esac
  done
fi

$SUDO chown 100:101 database

cat <<EOF
Please execute the following command in another shell, but in the same directory.
This will start backend (and database, which is its dependency) in interactive mode so that you can see the logging
output of the backend container in your shell.

  $SUDO docker-compose up backend

After you've done this and no more log messages appear **PRESS ENTER** to import data from the b-tu backend.

Useful commands:

* If you want to see logs from database container in real time, you can use the following command:

    $SUDO docker-compose logs -f database

* In case you don't need the containers to be running anymore, you can also stop them using docker-compose command (example for database container):

    $SUDO docker-compose stop database

* If you want to test the functionality of the "minimal-connector", use the following command.

    $SUDO docker-compose up minimal-connector

* In case you want to see how the data looks like in the app, start its corresponding container.

    $SUDO docker-compose up app
EOF

read _

while true; do
  read -p "How much do you want to import? (A)ll, (M)inimal or (N)othing? " yn
  case $yn in
      [Aa]* ) CHOICE="a"; break ;;
      [Mm]* ) CHOICE="m"; break ;;
      [Nn]* ) CHOICE="n"; break ;;
      * ) echo "Please choose A, M or N!";;
  esac
done

if [ "$CHOICE" = "n" ]; then
  exit;
fi

echo "This is currently not supported, because there is no public deployment for the app v2.0.0 by now."
exit 1
$SUDO docker-compose run --rm api copy --appVersion 2.0.0 place https://stappsbe01.innocampus.tu-berlin.de http://backend:3000 20
$SUDO docker-compose run --rm api copy --appVersion 2.0.0 dish https://stappsbe01.innocampus.tu-berlin.de http://backend:3000 20

if [ "$CHOICE" = "m" ]; then
  exit;
fi

$SUDO docker-compose run --rm api copy --appVersion 2.0.0 article https://stappsbe01.innocampus.tu-berlin.de http://backend:3000 20
$SUDO docker-compose run --rm api copy --appVersion 2.0.0 floor https://stappsbe01.innocampus.tu-berlin.de http://backend:3000 20
$SUDO docker-compose run --rm api copy --appVersion 2.0.0 organization https://stappsbe01.innocampus.tu-berlin.de http://backend:3000 20
$SUDO docker-compose run --rm api copy --appVersion 2.0.0 person https://stappsbe01.innocampus.tu-berlin.de http://backend:3000 20
$SUDO docker-compose run --rm api copy --appVersion 2.0.0 catalog https://stappsbe01.innocampus.tu-berlin.de http://backend:3000 20
$SUDO docker-compose run --rm api copy --appVersion 2.0.0 event https://stappsbe01.innocampus.tu-berlin.de http://backend:3000 20
$SUDO docker-compose run --rm api copy --appVersion 2.0.0 date https://stappsbe01.innocampus.tu-berlin.de http://backend:3000 20
